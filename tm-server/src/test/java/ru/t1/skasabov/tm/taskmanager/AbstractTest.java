package ru.t1.skasabov.tm.taskmanager;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.service.ConnectionService;
import ru.t1.skasabov.tm.service.PropertyService;

public abstract class AbstractTest {

    @NotNull
    protected static final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected static final IConnectionService connectionService = new ConnectionService(propertyService);

}
