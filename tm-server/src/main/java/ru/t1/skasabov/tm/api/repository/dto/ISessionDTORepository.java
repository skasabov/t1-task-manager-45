package ru.t1.skasabov.tm.api.repository.dto;

import ru.t1.skasabov.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {

}
