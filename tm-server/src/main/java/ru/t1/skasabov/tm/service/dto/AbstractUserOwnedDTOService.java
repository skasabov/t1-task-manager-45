package ru.t1.skasabov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.skasabov.tm.dto.model.AbstractUserOwnedModelDTO;

public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTOService<M> implements IUserOwnedDTOService<M> {

    protected AbstractUserOwnedDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
